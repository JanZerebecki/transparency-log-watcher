# Transparency Log Watcher

Logs incorrect answers from Trillian transparency logs into a
transparency log, so clients can know a log failed.

Status: Planning. No code, yet.

## What are transparency logs

Transparency logs are an example of verifiable data structures that
make any tampering evident. There are also other verifiable data
structures besides logs, so despite the name, this project isn't only
about logs. An implementation of it is
[Trillian](https://github.com/google/trillian) (see also the [Trillian
Website](https://transparency.dev/)).

While this project focuses on Trillian, its concepts are generally
applicable.

There are many applications for transparency logs. Among them Trillian
also implements a mode compatible with Certificate Transparency. Which
are logs for all HTTPS certificates from public Certificate
Authorities.

For this project making software package repositories more verifiable
is of particular interested, though it will probably also work for
other use cases. This interest includes among others source history
transparency (to ensure an attacker can't retroactively compromise
past changes and to be able to notice when different people think the
same version of a source package has different content, when only some
copies were compromised), [reproducible
builds](https://reproducible-builds.org/), and [distributed code
review like with cargo-crev](https://github.com/crev-dev/cargo-crev).

## Why transparency logs need to be watched

Just logging to a transparency log and checking if an entry is
correctly included in it, does not make the log append only. Just a
directory with signed files would have afforded a similar amount of
verifiability. The data structures and API used in Trillian make it
easier to verify than a director with signed files.

But each additional security property we want to rely on (like being
append only) needs to be verified by the user of the log. If a
verification fails, for some type of failures we want to appended
proof of the failure to a transparency log. This helps others to know
of the failure without the failure needing to have occurred to
them. This watchers-log is a transparency log to watch other
transparency logs.

The data structure or the responses of a transparency logs are often
signed, so that when a response is wrong the recipient has proof that
can convince someone else that the log failed.

E.g. if an HTTPS client found a certificate that is not in the logs it
would add a log entry of the certificate that has a valid CA signature
with the proof of non-inclusion from the responsible Certificate
Transparency logs to the watcher-logs, so that others can stop using
that CA based on this cryptographic proof.

Most properties need to be verified by every client. Some properties
need a full mirror of the log. A full-verifier with a mirror then
appends its conclusion to a watchers-log. Normal clients can then make
use of these conclusions.

## What to watch for

Some failure conditions are worth it to include in a watcher-log. This
heading should answer: Under what condition and how can a client
detect that another client noticed and proved that the log gave an
incorrect answer? When does this other client do the additional work
to notice this, if not every client does that work? Under which
condition does a client fail to detect that another client noticed an
incorrect answer from a log? Under which condition does no client
detect that an incorrect answer was given from a log?

TODO: These are only examples of failures. The above questions are not
yet answered.

1. Another client has seen a rollback compared to previous responses.
   (Rollback means here any operation that was not merely an append,
   like a truncate, or it could also be the result of a split-view
   attack.)  Every client should keep the state necessary to detect
   this.
2. A full mirroring log found an inconsistency while walking the
   tree. TODO: List the possible inconsistencies.
3. Another client has seen a response that then isn't included in the
   tree.
4. Another client has seen a split-view when comparing to a mirror.
   TODO: How should these mirrors be found, how should the ones to
   check be selected?
5. Another client has seen a split-view when comparing with another
   clients state. TODO: How and when should the client do this?
6. Another clients submission was accepted, but is then not included.
7. Another clients submission failed. Some failures do not result in a
   signed error, in that case the only way to proof it is to retry it
   with a watcher-log acting as a proxy for the submission to be able
   to verify that it failed. TODO: Can this work? Is there some amount
   of acceptable write unavailability during read availability,
   perhaps until any other write happens? There still remains the
   problem that no other party can verify this after the fact, though
   anyone can retry the submission.


TODO Are all read unavailabilities harmless (i.e. the client fails
temporarily and can retry indefinitely)?

TODO Admission criteria for the watcher-log. It is probably only
possible to accept proof for errors at a limited number of logs
identified by their public key, or is there some other way to prevent
DOS.

TODO: Each Trillian personality adds additional possibilities for
failure, list the ones for some personalities.

The [Trillian Gossip
Hub](https://github.com/google/trillian/blob/2aed2d75d64bf2c1f99322817af07d623b2b6de7/docs/Personalities.md#gossip-hub)
may have been intended to detect some of these, but the [Trillian
Gossip Hub was deleted without comment from the trillian-examples
Git](https://github.com/google/trillian-examples/pull/218).

## Alternatives to Trillian

An overview of other ways to achieve these goals.

### Architecturally different concepts

1. Blockchains like Bitcoin also use a Merkle tree like
   Trillian. However they are only designed for agreement on one view
   of their data to prevent double spending instead of history
   preservation, see the so called 51%-attack. Compared to >50% honest
   Blockchain nodes, Trillian just needs one honest log mirror or
   watcher-log. However when Trillian is easier or harder to
   compromise than a Blockchain depends on many other factors.

### Architecturally similar implementations or users of Trillian

1. The [reproducible build log for
   Debian](https://salsa.debian.org/reproducible-builds/transparency/log)
   uses Trillian. There were previous iterations that didn't use
   Trillian, but implemented a similar design.
2. [Sigstore Rekor is a signature transparency
   log](https://github.com/sigstore/rekor), that supports rpm and uses
   Trillian.
3. [Gossamer](https://gossamer.tools/) builds on [Chronicle](https://github.com/paragonie/chronicle),
   which is similar to Trillian. Claims to be append-only, but doesn't
   mention that the client will not detect various behaviour that is
   not append-only.

